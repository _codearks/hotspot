////
////  ViewController.swift
////  Hotspot
////
////  Created by suraj on 9/10/18.
////  Copyright © 2018 suraj. All rights reserved.
////
//
import UIKit
import UserNotifications
import NetworkExtension
import SystemConfiguration.CaptiveNetwork
//
class ViewController: UITableViewController, UNUserNotificationCenterDelegate  {
//
//    let wifiList:[String] = ["WORLDLINK WIFI", "Free Wlink", "FREE WORLDLINK CAFE WIFI", "Worldlink Wi Fi-project", "dhobighat_wlink"]
//    var wifiHotSpot = [String]()
//    var finalList = [String]()
//    let wifiStrength:[Int] = [3, 4, 2, 1, 5]
//    var model: WifiListModel?
//    var ssid = [String]()
//    var bssid = [String]()
//    var signal = [Double]()
//
//    var availableHotspot = [NEHotspotNetwork]()
//    var sortedHotspot = [NEHotspotNetwork]()
//    var highStrength = NEHotspotNetwork()
//
//    @IBAction func scanWifiAction(_ sender: UIButton) {
//        openWifiSettings()
//    }
//
//    // For foreground notification
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//
//        //displaying the ios local notification when app is in foreground
//        completionHandler([.alert, .badge, .sound])
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        //requesting for authorization
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
//
//        })
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//
//    }
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // Return the number of sections.
//        return 2
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // Return the number of rows in the section.
//        //      print(mac.count)
//        if(section == 0){
//            return 1
//        }else{
//            return availableHotspot.count
//        }
//    }
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 1.0
//    }
//
//    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 1.0
//    }
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if(indexPath.section == 1){
//            let configuration = NEHotspotConfiguration.init(ssid: "WORLDLINK WIFI", passphrase: "0123456789", isWEP: false)
//            configuration.joinOnce = true
//
//            NEHotspotConfigurationManager.shared.apply(configuration) { (error) in
//                if error != nil {
//                    if error?.localizedDescription == "already associated."
//                    {
//                        print("Connected")
//                        Notification().createNotification(message: "Log in is required to connect to network.")
//                    }
//                    else{
//                        print("No Connected")
//                    }
//                }
//                else {
//                    print("Connected here")
//                    Notification().createNotification(message: "Log in is required to connect to network.")
//                }
//            }
//        }
//
//    }
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 55
//    }
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if(indexPath.section == 0){
//            let cell = tableView.dequeueReusableCell( withIdentifier: "scanList", for :indexPath)
//            return cell
//        }else{
//            let cell = tableView.dequeueReusableCell( withIdentifier: "wifiList", for :indexPath)
//            (cell as! WifiListCell).wifiName.text = availableHotspot[indexPath.row].ssid
//            return cell
//        }
//    }
//
//    func openWifiSettings() {
//        //open setting page here
//        let alertController = UIAlertController(title: "Important", message: "Opening wifi setting page is needed to scan the wifi.", preferredStyle: .alert)
//
//
//        // Setting button action
//        let settingsAction = UIAlertAction(title: "Go to Setting", style: .default) { (_) -> Void in
//            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
//                return
//            }
//
//            if UIApplication.shared.canOpenURL(settingsUrl) {
//                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                    // Checking for setting is opened or not
//                    print("Setting is opened: \(success)")
//                    self.scanWifiList()
//                })
//            }
//        }
//
//        alertController.addAction(settingsAction)
//        // Cancel button action
//        let cancelAction = UIAlertAction(title: "Cancel", style: .default){ (_) -> Void in
//            // Magic is here for cancel button
//        }
//        alertController.addAction(cancelAction)
//        // This part is important to show the alert controller ( You may delete "self." from present )
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    public func scanWifiList(){
//        let options: [String: NSObject] = [kNEHotspotHelperOptionDisplayName : "Worldlink Hotspot" as NSObject]
//        let queue: DispatchQueue = DispatchQueue(label: "np.com.worldlink.wifihotspot", attributes: DispatchQueue.Attributes.concurrent)
//
//        NSLog("Started wifi list scanning.")
//
//        NEHotspotHelper.register(options: options, queue: queue) { (cmd: NEHotspotHelperCommand) in
//            NSLog("Received command: \(cmd.commandType.rawValue)")
//
//            if cmd.commandType == NEHotspotHelperCommandType.filterScanList {
//                //Get all available hotspots
//                let list: [NEHotspotNetwork] = cmd.networkList!
//                self.availableHotspot.removeAll()
//                self.getBestScanResult(list: list)
//                //Figure out the hotspot you wish to connect to
//                let desiredNetwork : NEHotspotNetwork? = self.getHighSignalResult(list: list)
//                if let network = desiredNetwork {
////                    network.setPassword("") //Set the WIFI password
//                    let response = cmd.createResponse(NEHotspotHelperResult.success)
//
//                    response.setNetworkList([network])
//                    response.deliver() //Respond back with the filtered list
//                }
//            } else if cmd.commandType == NEHotspotHelperCommandType.evaluate {
//                if let network = cmd.network {
//                    print("here1")
//                    //Set high confidence for the network
//                    network.setConfidence(NEHotspotHelperConfidence.high)
//
//                    let response = cmd.createResponse(NEHotspotHelperResult.success)
//                    response.setNetwork(network)
//                    response.deliver() //Respond back
//                }
//            } else if cmd.commandType == NEHotspotHelperCommandType.authenticate {
//                //Perform custom authentication and respond back with success
//                // if all is OK
//                print("here2")
//                let configuration = NEHotspotConfiguration.init(ssid: "FREE WORLDLINK CAFE WIFI")
//                configuration.joinOnce = true
//
//                NEHotspotConfigurationManager.shared.apply(configuration) { (error) in
//                    if error != nil {
//                        if error?.localizedDescription == "already associated."
//                        {
//                            print("Connected here")
//                            let response = cmd.createResponse(NEHotspotHelperResult.success)
//                            response.deliver() //Respond back
//                            Notification().createNotification(message: "here Log in is required to access the internet.")
//                        }
//                        else{
//                            print("No Connected")
//                        }
//                    }
//                    else {
//                        print("Connected here")
//                        let response = cmd.createResponse(NEHotspotHelperResult.success)
//                        response.deliver() //Respond back
//                        Notification().createNotification(message: "here Log in is required to access the internet.")
//                    }
//                }
//            }
//        }
//    }
//
//    func getBestScanResult(list: [NEHotspotNetwork]){
//        //        let connection = Connection(url: "https://www.google.com.np",parameters:[
//        //            "abc":"abc",
//        //            ])
//
//        self.wifiHotSpot = ["abc", "cba", "WORLDLINK WIFI", "def", "aaa", "Free Wlink", "ccc", "ddd", "eee", "FREE WORLDLINK CAFE WIFI", "fff" ,"Worldlink Wi Fi-projet"]
//
//        for i in 0..<list.count {
//            if(self.wifiHotSpot.contains(list[i].ssid)) {
//                if !availableHotspot.contains(list[i]){
//                 availableHotspot.append(list[i])
//                }
//            }
//        }
////        for j in 0..<availableHotspot.count {
////            for k in 1..<availableHotspot.count {
////                if(availableHotspot[j].signalStrength > availableHotspot[k].signalStrength){
////                    highStrength = availableHotspot[j]
////
////                }
////            }
////            print("FinalListed sort \(highStrength)")
////            sortedHotspot.append(highStrength)
////        }
//        self.tableView.reloadData()
////        print("FinalListed \(availableHotspot)")
////        print("FinalListed 123\(sortedHotspot)")
//
//        //        connection.start { (result, success) in
//        //            if success {
//        //                self.wifiHotSpot = ["abc", "cba", "WORLDLINK WIFI", "def", "aaa", "Free Wlink", "ccc", "ddd", "eee", "FREE WORLDLINK CAFE WIFI", "fff" ,"Worldlink Wi Fi-project"]
//        //                for i in 0..<self.wifiList.count {
//        //                    if(self.wifiHotSpot.contains(self.wifiList[i])) {
//        //                        self.finalList.append(self.wifiList[i])
//        //                    }
//        //                }
//        //                print("FinalList \(self.finalList)")
//        //            }else{
//        //                Alert().showAlert(self, msg: "Something went wrong, Please try again", okAction:{() in
//        //                })
//        //            }
//        //        }
//    }
//    func getHighSignalResult(list: [NEHotspotNetwork]) -> NEHotspotNetwork{
//        self.wifiHotSpot = ["abc", "cba", "WORLDLINK WIFI", "def", "aaa", "Free Wlink", "ccc", "ddd", "eee", "FREE WORLDLINK CAFE WIFI", "fff" ,"Worldlink Wi Fi-projet"]
//        var networkList = NEHotspotNetwork()
//        for i in 0..<list.count {
//            if(self.wifiHotSpot.contains(list[i].ssid) && list[i].ssid == "FREE WORLDLINK CAFE WIFI") {
//                for j in 1..<list.count {
//                    if(list[i].signalStrength > list[j].signalStrength){
//                        networkList = list[i]
//                    }
//                }
//            }
//        }
//        let configuration = NEHotspotConfiguration.init(ssid: networkList.ssid)
//        configuration.joinOnce = true
//
//        NEHotspotConfigurationManager.shared.apply(configuration) { (error) in
//            if error != nil {
//                if error?.localizedDescription == "already associated."
//                {
//                    print("Connected")
//                    Notification().createNotification(message: "Log in is required to access the internet.")
//                }
//                else{
//                    print("No Connected")
//                }
//            }
//            else {
//                print("Connected here")
//                Notification().createNotification(message: "Log in is required to access the internet.")
//            }
//        }
//        print("FinalList1 \(networkList)")
//        return networkList
//    }
}
