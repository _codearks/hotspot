//
//  WifiListModel.swift
//  Hotspot
//
//  Created by suraj bhandari on 9/24/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
import NetworkExtension
import SystemConfiguration.CaptiveNetwork

struct WifiListModel {
    
    var ssid = [String]()
    var bssid = [String]()
    var signal = [Double]()
    
    init(_ decode : [NEHotspotNetwork]){
        for i in 0..<decode.count {
            ssid.append(decode[i].ssid)
            bssid.append(decode[i].bssid)
            signal.append(decode[i].signalStrength)
        }
    }
}
