//
//  WifiListCell.swift
//  Hotspot
//
//  Created by suraj bhandari on 9/18/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
import UIKit

class WifiListCell: UITableViewCell {
    
    @IBOutlet weak var wifiName: UILabel!
    @IBOutlet weak var strengthView: UIImageView!
    
}
