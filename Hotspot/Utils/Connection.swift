//
//  Connection.swift
//  Hotspot
//
//  Created by suraj bhandari on 9/17/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol LoaderCallback : class {
    func onDataLoad(_ json: JSON)
}

class Connection {
    
    var url: String
    var parameters = [String:String]()
    var headers = [String:String]()
    let callback: LoaderCallback?
    
    init(url: String, callback: LoaderCallback, parameters: [String:String], headers: [String:String]) {
        self.url = url
        self.callback = callback
        self.parameters = parameters
        self.headers = headers
    }
    
    init(url:String, parameters: [String:String], headers: [String:String]) {
        self.url = url
        self.callback = nil
        self.parameters = parameters
        self.headers = headers
    }
    
    
    func start(_ success : @escaping (_ result:JSON, _ success : Bool) -> Void ) {
        let myresponse = Alamofire.request(url, method: .post, parameters: self.parameters, encoding: URLEncoding.httpBody, headers: self.headers)
        print("Parameters \(self.parameters)")
        print("Headers \(self.headers)")
        myresponse.responseJSON() {
            (response) in
            guard response.result.isSuccess else {
                success(JSON.null, false)
                return
            }
            if let data = response.data {
                do {
                    let result = try JSON(data: data)
                    success(result, true)
                } catch {
                    print(error)
                    // or display a dialog
                }
            }
        }
    }
    
    func fetch(_ callback : @escaping (_ result:String?, _ success: Bool) -> Void ) {
        let myrespone = Alamofire.request(self.url, method: .post, parameters: self.parameters)
        myrespone.responseJSON() {
            (response) in
            if let data = response.data {
                if let result =  String(data: data, encoding: String.Encoding.utf8) {
                    callback(result, true)
                }
            }else{
                callback(nil, false)
            }
        }
    }
    
    func load(_ success : @escaping (_ result:JSON, _ success : Bool) -> Void ) {
        let myresponse = Alamofire.request(self.url, method: .get)
        myresponse.responseJSON() {
            (response) in
            guard response.result.isSuccess else {
                success(JSON.null, false)
                return;
                
            }
            if let data = response.data {
                do {
                    let result = try JSON(data: data)
                    success(result, true)
                } catch {
                    print(error)
                    // or display a dialog
                }
            }
            
        }
    }
}
