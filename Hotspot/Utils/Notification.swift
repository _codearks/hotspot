//
//  Notification.swift
//  Hotspot
//
//  Created by suraj bhandari on 9/18/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class Notification {
    
    public func createNotification(message: String){
        //creating the notification content
        let content = UNMutableNotificationContent()
        
        //adding title, subtitle, body and badge
        content.title = message
        //        content.subtitle = "iOS Development is fun"
        //        content.body = "We are learning about iOS Local Notification"
        content.badge = 1
        
        //getting the notification trigger
        //it will be called after 1 seconds
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        //getting the notification request
        let request = UNNotificationRequest(identifier: "HotSpotNotification", content: content, trigger: trigger)
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    public func openNotification(_ this: UIViewController){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "captive_home")
        this.present(controller, animated: true, completion: nil)
//        if let url = URL(string: "https://www.google.com.np") {
//            UIApplication.shared.open(url, options: [:])
//        }
    }
}
