//
//  AppDelegateConstants.swift
//  Hotspot
//
//  Created by suraj bhandari on 9/27/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation

enum AppDelegatConstants: String {
    case applicationDidBecomeActive
}
