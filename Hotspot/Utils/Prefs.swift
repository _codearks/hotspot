//
//  Prefs.swift
//  Hotspot
//
//  Created by suraj bhandari on 10/23/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
class Prefs {
    internal static let mac_id = "ID"
    internal static let mac_ssid = "SSID"
    internal static let mac_bssid = "BSSID"
    internal static let username = "USERNAME"
    internal static let password = "PASSWORD"
    internal static let user_token = "USER_TOKEN"
    
    //store string
    static func storeString(_ key:String, value:String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    //retrieve
    static func retrieveString(_ key:String) -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: key)
    }
    
    //store array
    static func storeArray(_ key:String, value:Array<AnyObject>){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    //retrieve
    static func retrieveArray(_ key:String) -> Array<AnyObject>{
        let defaults = UserDefaults.standard
        return defaults.array(forKey: key)! as Array<AnyObject>
    }
}
