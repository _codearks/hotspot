//
//  CaptiveLoginController.swift
//  Hotspot
//
//  Created by suraj bhandari on 9/27/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import Reachability

class CaptiveLoginController: UIViewController, URLSessionDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var loginView: UIButton!
    
    @IBOutlet weak var messageBar: UIView!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var messageStatus: UILabel!
    @IBOutlet weak var messageDesc: UILabel!
    
    var usernameValue = String()
    var passwordValue = String()
    
    var reachability: Reachability?
    let hostNames = ["https://www.google.com.np"]
    var hostIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.isHidden = true
        spinner.transform = CGAffineTransform(scaleX: 2, y: 2)
        view.addSubview(scrollView)
        messageBar.isHidden = true
        let uimage = UIImage(named: "user")
        username.setIcon(uimage!)
        let pimage = UIImage(named: "lock")
        password.setIcon(pimage!)
        setTextFieldBorder([username, password])
        username.addDoneButtonOnKeyboard()
        password.addDoneButtonOnKeyboard()
        username.attributedPlaceholder = NSAttributedString(string: "Username",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        password.attributedPlaceholder = NSAttributedString(string: "Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotate
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor(hexString: "#0771B9")
        NVActivityIndicatorView.DEFAULT_PADDING = CGFloat(5.0)
        NVActivityIndicatorView.DEFAULT_TEXT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        
        self.hideKeyboardWhenTappedAround()
//        startAnimating(message: "Connecting to hotspot, Please wait")
        startHost(at: 0)
        loginView.layer.cornerRadius = 8
        
        //automatic login from preference if exists.
        spinner.isHidden = false
        spinner.startAnimating()
//        if(Prefs.retrieveString(Prefs.username) != nil){
//            if(NetworkReach.isConnectedToNetwork()){
//                usernameValue = Prefs.retrieveString(Prefs.username)!
//                passwordValue = Prefs.retrieveString(Prefs.password)!
//                print("usernema \(usernameValue)")
//                print("usernema \(passwordValue)")
//                getTokenFromRedirectUrl()
//            }else{
//                enableUserInput(data: true)
//                spinner.stopAnimating()
//                spinner.isHidden = true
//                //                stopAnimating()
//                messageBar.isHidden = false
//                messageStatus.text = "ERROR"
//                messageDesc.text = "Internet Connection Error."
//            }
//        }else{
//            enableUserInput(data: true)
//        }
    }
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        scrollView.contentSize = CGSize(width: 375, height: 800)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "home_view" {
            if let destinationVC = segue.destination as? UINavigationController {
                _ = destinationVC.viewControllers[0] as! HomeViewController
            }
        } else if segue.identifier == "success_view" {
            if let destinationVC = segue.destination as? UINavigationController {
                _ = destinationVC.viewControllers[0] as! MessageViewController
            }
        } else if segue.identifier == "captive_login" {
            if let destinationVC = segue.destination as? UINavigationController {
                _ = destinationVC.viewControllers[0] as! CaptiveLoginController
            }
        }
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        messageBar.isHidden = true
        usernameValue = username.text!
        passwordValue = password.text!
        if(usernameValue != "" && passwordValue != ""){
//            startAnimating(message: "Validating, Please wait.")
            spinner.isHidden = false
            spinner.startAnimating()
            if(NetworkReach.isConnectedToNetwork()){
                getTokenFromRedirectUrl()
            }else{
                spinner.isHidden = true
//                stopAnimating()
                messageBar.isHidden = false
                messageStatus.text = "ERROR"
                messageDesc.text = "Internet Connection Error."
            }
        }else{
//            Alert().showAlert(self, msg: "Please fill all the fields", okAction:{() in
//            })
            spinner.isHidden = true
            messageBar.isHidden = false
            messageStatus.text = "ERROR"
            messageDesc.text = "Fields cannot be empty."
        }
    }
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
//        self.dismiss(animated: true, completion: nil)
        self.performSegue(withIdentifier: "home_view", sender: self)
    }
    func captiveLogin(token: String) {
        //182 is a auth id
        let connection = Connection(url: "https://en01.wlink.com.np/papi/hotspot/auths/182/user",parameters:[
                        "username": usernameValue,
                        "password": passwordValue,
                        "user_token": token //get from redirect url
            ], headers:[
                "Content-Type": "application/x-www-form-urlencoded"
            ])
                connection.start { (result, success) in
                    if success {
                        print("Json \(result)")
                        if result["result"].isEmpty {
                            self.enableUserInput(data: true)
                            self.spinner.isHidden = true
                            let message = result["error"]["message"].stringValue
                            self.messageBar.isHidden = false
                            self.messageStatus.text = "ERROR"
                            self.messageDesc.text = message
//                            Alert().showAlert(self, msg: message, okAction:{() in
//                            })
                        }else{
                            if(result["result"]["success"].boolValue == true){
                                let loginUrl = result["result"]["login_url"].stringValue
                                let token = result["result"]["temp_token"].stringValue
                                self.captiveInternetAccess(urlString: loginUrl, token: token, username: self.usernameValue, password: self.passwordValue)
                            }else{
                                self.enableUserInput(data: true)
                                self.spinner.isHidden = true
                                self.messageBar.isHidden = false
                                self.messageStatus.text = "ERROR"
                                self.messageDesc.text = "Network error. Please try again later."
//                                Alert().showAlert(self, msg: "Network error.", okAction:{() in
//                                })
                            }
                        }
                    }else{
                        self.enableUserInput(data: true)
                        self.spinner.isHidden = true
                        self.messageBar.isHidden = false
                        self.messageStatus.text = "ERROR"
                        self.messageDesc.text = "Something went wrong, Please try again."
//                        Alert().showAlert(self, msg: "Something went wrong, Please try again", okAction:{() in
//                        })
                    }
                }
    }
    
    func captiveInternetAccess(urlString: String, token: String, username: String, password: String) {
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postString = "username=" + token + "&password=" + token + "&url=https://jsonplaceholder.typicode.com/posts/1"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                self.messageBar.isHidden = false
                self.messageStatus.text = "ERROR"
                self.messageDesc.text = "Something went wrong, Please try again."
//                Alert().showAlert(self, msg: "Something went wrong, Please try again", okAction:{() in
//                })
                return
            }
    
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                self.enableUserInput(data: true)
                self.messageBar.isHidden = false
                self.messageStatus.text = "ERROR"
                self.messageDesc.text = "Network Error, Please try again."
//                Alert().showAlert(self, msg: "Network Error, Please try again", okAction:{() in
//                })
            }
            let responseString = String(data: data, encoding: .utf8)
            print("Response String \(responseString)")
            Prefs.storeString(Prefs.username, value: username)
            Prefs.storeString(Prefs.password, value: password)
            Prefs.storeString(Prefs.user_token, value: token)
            self.stopNotifier()
            self.performSegue(withIdentifier: "success_view", sender: self)
        }
        task.resume()
    }
    
    func getTokenFromRedirectUrl() {
        enableUserInput(data: false)
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirectionWithCompletion = { session, task, response, request, completion in
            // request.URL has the redirected URL inside of it, no need to parse the body
            if let url = request.url {
                print("Extracted URL: \(url)")
                //For multiple redirection start
                Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirectionWithCompletion = { session, task, response, request, completion in
                    // request.URL has the redirected URL inside of it, no need to parse the body
                    if let url = request.url {
                        print("Extracted URL: \(url)")
                        if(url.absoluteString.contains("token")){
                            let tokenString:[String] = url.absoluteString.components(separatedBy: "token=")[1].components(separatedBy: "&")
                            let token = tokenString[0]
                            self.captiveLogin(token: token)
                        }
                    }
                }
                
                // We expect a redirect, so the completion of this call should never execute
                let url = URL(string: url.absoluteString)
                let request = Alamofire.request(url!, method: .get, parameters: [:], encoding: JSONEncoding.default, headers: [:])
                request.response { request in
                    print("Logic Error, response should NOT have been called for request: \(request)")
                    self.enableUserInput(data: true)
//                    Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = nil // Restore redirect abilities - just in case
                }
                //Multiple redirection end
                
            }
        }

        // We expect a redirect, so the completion of this call should never execute
        let url = URL(string: "http://captive.apple.com/hotspot-detect.html")
        let request = Alamofire.request(url!, method: .get, parameters: [:], encoding: JSONEncoding.default, headers: [:])
        request.response { request in
            print("Logic Error, response should NOT have been called for request: \(request)")
            self.spinner.isHidden = true
            if(NetworkReach.isConnectedToNetwork()){
                self.performSegue(withIdentifier: "success_view", sender: self)
            }else{
                self.messageBar.isHidden = false
                self.messageStatus.text = "ERROR"
                self.messageDesc.text = "Something went wrong, Please try again."
                self.enableUserInput(data: true)
//                Alert().showAlert(self, msg: "Something went wronged, Please try again", okAction:{() in })
            }
//            Alert().showAlert(self, msg: "Something went wronged, Please try again", okAction:{() in
//            })
//            Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = nil // Restore redirect abilities - just in case
        }
    }
    
    func startHost(at index: Int) {
        stopNotifier()
        setupReachability(hostNames[index], useClosures: true)
        startNotifier()
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        //            self.startHost(at: (index + 1) % 3)
        //        }
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
        } else {
            reachability = Reachability()
        }
        self.reachability = reachability
        
        if useClosures {
            reachability?.whenReachable = { reachability in
                print("reachable")
                self.spinner.isHidden = true
            }
            reachability?.whenUnreachable = { reachability in
                print("unreachable")
                if let wd = UIApplication.shared.delegate?.window {
                    var vc = wd!.rootViewController
                    if(vc is UINavigationController){
                        vc = (vc as! UINavigationController).visibleViewController
                    }
                    
                    if(vc is CaptiveLoginController){
                        print("here1234")
                        //your code
                        if(!NetworkReach.isConnectedToNetwork()){
                            print("here12345")
                            self.spinner.isHidden = false
                            self.spinner.startAnimating()
//                            self.startAnimating(message: "Connecting to hotspot, Please wait")
                        }
                    }else{
                        if(!NetworkReach.isConnectedToNetwork()){
                            self.spinner.isHidden = false
                            self.spinner.startAnimating()
//                            self.startAnimating(message: "Connecting to hotspot, Please wait")
                        }
//                        self.dismiss(animated: true, completion: nil)
                    }
                }
                
            }
        }
    }
    
    func setHostSpotAPList() {
        //182 is a auth id
        let connection = Connection(url: "https://ssl.worldlink.com.np:444/custmobileapp/index.php/main/getHotspotMacList",parameters:[:], headers:[:])
        connection.start { (result, success) in
            if success {
                if(result["status"] == 1){
                    var idArray = [String]()
                    var ssidArray = [String]()
                    var bssidArray = [String]()
                    let data = result["data"].arrayValue
                    for i in 0..<data.count {
                        idArray.append(data[i]["id"].stringValue)
                        ssidArray.append(data[i]["ssid"].stringValue)
                        bssidArray.append(data[i]["bssid"].stringValue)
                    }
                }
            }else{
//                Alert().showAlert(self, msg: "Something went wrong, Please try again", okAction:{() in
//                })
            }
        }
    }
    func enableUserInput(data: Bool){
        username.isEnabled = data
        password.isEnabled = data
        loginView.isEnabled = data
        username.isUserInteractionEnabled = data
        password.isUserInteractionEnabled = data
        loginView.isUserInteractionEnabled = data
    }
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
            print("catch here")
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        reachability?.stopNotifier()
        reachability = nil
    }
    
    func setTextFieldBorder(_ textField: [UITextField]) {
        for i in 0..<textField.count{
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(hexString: "#757575").cgColor
        border.frame = CGRect(x: 0, y: textField[i].frame.size.height - width, width: UIScreen.main.bounds.width - 32, height: textField[i].frame.size.height)
        border.borderWidth = width
        textField[i].layer.addSublayer(border)
        textField[i].layer.masksToBounds = true
        }
    }
    
}
