//
//  MessageViewController.swift
//  Hotspot
//
//  Created by suraj bhandari on 10/23/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
import UIKit

class MessageViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onResume),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil)
    }
    @objc func onResume() {
        if let window = UIApplication.shared.delegate?.window {
            if var viewController = window?.rootViewController {
                // handle navigation controllers
                if(viewController is UINavigationController){
                    viewController = (viewController as! UINavigationController).visibleViewController!
                }
                if(!viewController.isKind(of: HomeViewController.self)){
                    performSegue(withIdentifier: "unwindToHome", sender: self)
                }
            }
        }
    }
    @IBAction func closeAppAction(_ sender: UIButton) {
//        UIControl().sendAction(#selector(NSXPCConnection.suspend),
//                               to: UIApplication.shared, for: nil)
        
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        // terminaing app in background
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            exit(EXIT_SUCCESS)
        })
    }
    
}
