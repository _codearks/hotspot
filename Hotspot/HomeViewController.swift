//
//  HomeViewController.swift
//  Hotspot
//
//  Created by suraj bhandari on 9/27/18.
//  Copyright © 2018 suraj. All rights reserved.
//

import Foundation
import UIKit
import NetworkExtension
import UserNotifications
import SystemConfiguration.CaptiveNetwork

class HomeViewController: UIViewController {
    var wifiHotSpot = ["28:76:10:17:99:e8", "28:76:10:17:a5:0a", "28:76:10:17:a1:a4", "28:76:10:17:95:ab","28:76:10:17:ad:e8", "28:76:10:17:9f:d8", "28:76:10:00:56:a4", "28:76:10:17:91:64", "28:76:10:17:aa:c3", "28:76:10:17:ae:ba", "28:76:10:17:81:83", "28:76:10:17:b9:fa", "28:76:10:17:b4:37", "28:76:10:17:92:4a", "28:76:10:17:82:af", "28:76:10:17:ae:0b", "28:76:10:17:a8:9d", "28:76:10:17:ae:42", "28:76:10:17:a7:b2", "28:76:10:17:b4:e6", "28:76:10:17:ac:bc", "28:76:10:17:be:e1", "28:76:10:17:aa:b9", "28:76:10:17:9d:1c", "28:76:10:17:b5:45", "28:76:10:17:b4:4b", "28:76:10:17:b8:2e", "28:76:10:17:9e:ac", "28:76:10:17:91:0a", "28:76:10:17:79:a4", "28:76:10:17:a8:16", "28:76:10:17:ba:54", "28:76:10:17:aa:37", "28:76:10:17:ae:56", "28:76:10:17:b5:90", "28:76:10:17:ae:29", "28:76:10:17:b6:03", "28:76:10:17:98:26", "28:76:10:17:ac:80", "28:76:10:17:ab:0e", "28:76:10:17:a2:3a", "28:76:10:17:9b:14", "28:76:10:17:9b:28", "28:76:10:13:83:5a", "28:76:10:17:b9:9b", "28:76:10:17:9d:2b", "28:76:10:17:a0:c3", "28:76:10:17:be:d7", "28:76:10:17:b7:34", "28:76:10:17:8a:ca", "28:76:10:17:90:88", "28:76:10:17:c1:2f", "28:76:10:17:b1:d0", "28:76:10:17:87:3c", "28:76:10:17:b6:3a", "28:76:10:17:a8:11", "28:76:10:17:78:46", "28:76:10:17:93:7b", "28:76:10:17:78:a0", "28:76:10:17:aa:28", "28:76:10:17:b9:5f", "28:76:10:17:b1:2b", "2C:76:10:17:84:5", "2C:76:10:17:84:6", "2e:76:10:14:0f:de", "2e:76:10:14:0f:da", "28:76:10:13:84:f4"]
    
    var wifiNetwork = NEHotspotNetwork()
    var isSSIDAvailable:Bool = false
    var isNetworkVisible:Bool = false
    var isScanningComplete:Bool = false
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var spinnerText: UILabel!
    @IBOutlet weak var tryAgain: UIButton!
    
    
    // For foreground notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    @IBAction func tryAgainAction(_ sender: UIButton) {
        spinner.isHidden = false
        spinner.startAnimating()
        spinnerText.text = "Scanning Wifi Hotspot"
        if let url = URL(string:"App-Prefs:root=WIFI") {
            if UIApplication.shared.canOpenURL(url) {
                tryAgain.isHidden = true
                self.scanWifiList()
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.isHidden = true;
        spinner.transform = CGAffineTransform(scaleX: 2, y: 2)
//        Prefs.storeArray(Prefs.mac_bssid, value: wifiHotSpot as Array<AnyObject>)
        //        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.onResume), name: NSNotification.Name(rawValue: AppDelegatConstants.applicationDidBecomeActive.rawValue), object: nil)
        
        //requesting for authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
            
        })
        print("IsConnected \(NetworkReach.isConnectedToNetwork())")
        if NetworkReach.isConnectedToNetwork(){
            spinner.isHidden = true
            spinnerText.text = "You are already connected to network."
            tryAgain.isHidden = false
            tryAgain.setTitle("Try another network", for: .normal)
        }else{
            tryAgain.isHidden = false
            spinner.isHidden = true
            tryAgain.setTitle("Go to wifi settings", for: UIControlState.normal)
            spinnerText.text = "myWorldlink needs to scan wifi list to connect to hotspot."
//            openWifiSettings()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onResume),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil)
    }
    
    @IBAction func unwindToViewController(segue: UIStoryboardSegue) {
    }
    
    @objc func onResume() {
        print("on resumed")
        print("IsSSID Available \(isSSIDAvailable)")
        if NetworkReach.isConnectedToNetwork() && !isSSIDAvailable{
            spinner.isHidden = true
            spinnerText.text = "You are already connected to network."
            tryAgain.setTitle("Try another network", for: .normal)
            tryAgain.isHidden = false
        }else if(isSSIDAvailable && wifiNetwork.ssid != ""){
            print("WIFI \(wifiNetwork)")
            let configuration = NEHotspotConfiguration.init(ssid: wifiNetwork.ssid)
            configuration.joinOnce = true
            if(isNetworkVisible){
                self.spinner.isHidden = true
                self.spinnerText.text = "You are already connected to network."
                self.tryAgain.setTitle("Try another network", for: UIControlState.normal)
                self.tryAgain.isHidden = false
            }else{
                self.spinner.isHidden = false
                spinnerText.text = "Joining Network"
                self.tryAgain.isHidden = true
            }
            NEHotspotConfigurationManager.shared.apply(configuration) { (error) in
                if error != nil {
                    if error?.localizedDescription == "already associated." {
                        if(self.isNetworkVisible){
                        if let wd = UIApplication.shared.delegate?.window {
                            var vc = wd!.rootViewController
                            if(vc is UINavigationController){
                                vc = (vc as! UINavigationController).visibleViewController
                            }
                            
                            if(vc is HomeViewController){
                                print("Home here")
                                self.spinner.isHidden = false
                                self.tryAgain.isHidden = true
                                self.spinnerText.text = "Connecting to \(self.wifiNetwork.ssid). Please wait."
                            }
                        }
                        }else{
                            self.spinner.isHidden = true
                            self.spinnerText.text = "You are already connected to network."
                            self.tryAgain.setTitle("Try another network", for: UIControlState.normal)
                            self.tryAgain.isHidden = false
                            print("WIFI connected")
                        }
                    }
                    else{
                        self.spinner.isHidden = false
                        self.tryAgain.isHidden = true
                        print("WIFI not connected")
                    }
                }
                else {
                    self.spinnerText.text = "Connecting to \(self.wifiNetwork.ssid). Please wait."
                    self.isNetworkVisible = true
                    print("WIFI connected here")
                }
            }
        }else{
            tryAgain.isHidden = false
            spinner.isHidden = true
            tryAgain.setTitle("Go to settings", for: UIControlState.normal)
            spinnerText.text = "myWorldlink needs to scan wifi list to connect to hotspot."
//            openWifiSettings()
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "captive_portal" {
            if let destinationVC = segue.destination as? UINavigationController {
                let nextViewController = destinationVC.viewControllers[0] as! CaptiveLoginController
            }
        }else if segue.identifier == "message_view" {
            if let destinationVC = segue.destination as? UINavigationController {
                let nextViewController = destinationVC.viewControllers[0] as! MessageViewController
            }
        }
    }
    
    public func scanWifiList(){
        let options: [String: NSObject] = [kNEHotspotHelperOptionDisplayName : "Worldlink Hotspot" as NSObject]
        let queue: DispatchQueue = DispatchQueue(label: "np.com.worldlink.wifihotspot", attributes: DispatchQueue.Attributes.concurrent)
        
        NSLog("Started wifi list scanning.")
        
        NEHotspotHelper.register(options: options, queue: queue) { (cmd: NEHotspotHelperCommand) in
            NSLog("Received command: \(cmd.commandType.rawValue)")
            
            if cmd.commandType == NEHotspotHelperCommandType.filterScanList {
                //Get all available hotspots
                let list: [NEHotspotNetwork] = cmd.networkList!
                //Figure out the hotspot you wish to connect to
                let desiredNetwork : NEHotspotNetwork? = self.getHighSignalResult(list: list)
                if(self.isSSIDAvailable){
                    if let network = desiredNetwork {
                        //                                        network.setPassword("0123456789") //Set the WIFI password
                        let response = cmd.createResponse(NEHotspotHelperResult.success)
                        response.setNetworkList([network])
                        response.deliver() //Respond back with the filtered list
                    }
                }
            } else if cmd.commandType == NEHotspotHelperCommandType.evaluate {
                if let network = cmd.network {
                    print("here1")
                    //Set high confidence for the network
                    network.setConfidence(NEHotspotHelperConfidence.high)
                    
                    let response = cmd.createResponse(NEHotspotHelperResult.success)
                    response.setNetwork(network)
                    response.deliver() //Respond back
                }
            } else if cmd.commandType == NEHotspotHelperCommandType.authenticate {
                //Perform custom authentication and respond back with success
                // if all is OK
                let response = cmd.createResponse(NEHotspotHelperResult.success)
                response.deliver() //Respond back
                if(self.isSSIDAvailable && self.isNetworkVisible){
                    self.performSegue(withIdentifier: "captive_portal", sender: self)
                }
            }
        }
    }
    
    
    
    func getHighSignalResult(list: [NEHotspotNetwork]) -> NEHotspotNetwork{
//        let bssidList:[String] = Prefs.retrieveArray(Prefs.mac_bssid) as! [String]
        var networkList = NEHotspotNetwork()
        for i in 0..<list.count {
            if(wifiHotSpot.contains(list[i].bssid.uppercased())) {
                for j in 1..<list.count {
                    if(list[i].signalStrength > list[j].signalStrength){
                        networkList = list[i]
                        if(networkList.ssid != ""){
                            isSSIDAvailable = true
                        }else{
                            isSSIDAvailable = false
                        }
                    }
                }
            }
        }
        wifiNetwork = networkList
        print("IsSSId \(isSSIDAvailable)")
        print("IsSSId network \(wifiNetwork)")
        if(isSSIDAvailable) {
            print("Scan completed")
            Notification().createNotification(message: "Wifi list scan completed. Open app to connect to hotspot.")
        }else{
            print("Scan completed not found")
            Notification().createNotification(message: "Worldlink Hotspot wifi not found.")
        }
        print("FinalList1 \(networkList)")
        return networkList
    }
    //getHighSignal ends...
    
    //getting wifi connected details.
//    func getWIFIInformation() -> [String:String]{
//        var informationDictionary = [String:String]()
//        let informationArray:NSArray? = CNCopySupportedInterfaces()
//        if let information = informationArray {
//            let dict:NSDictionary? = CNCopyCurrentNetworkInfo(information[0] as! CFString)
//            if let temp = dict {
//                informationDictionary["SSID"] = temp["SSID"]! as? String
//                informationDictionary["BSSID"] = temp["BSSID"]! as? String
//                return informationDictionary
//            }
//        }
//
//        return informationDictionary
//    }
    
    
    @objc func openWifiSettings() {
        tryAgain.isHidden = false
        spinner.isHidden = true
        spinner.startAnimating()
        spinner.transform = CGAffineTransform(scaleX: 2, y: 2)
        spinnerText.text = "myWorldlink needs to scan wifi list to connect Hotspot."
        
        let alertController = UIAlertController (
            title: "Welcome to Worldlink Hotspot",
            message: "Go to (Settings -> Wifi) to scan the wifi hotspot",
            preferredStyle: .alert
        )
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            //            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            //                return
            //            }
            
            if let url = URL(string:"App-Prefs:root=WIFI") {
                if UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        self.scanWifiList()
                    } else {
                        UIApplication.shared.openURL(url)
                        self.scanWifiList()
                    }
                }
            }
            
            //            if UIApplication.shared.canOpenURL(settingsUrl) {
            //                if #available(iOS 10.0, *) {
            //                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
            //                        self.scanWifiList()
            //                    })
            //                } else {
            //                    UIApplication.shared.openURL(settingsUrl)
            //                    self.scanWifiList()
            //                }
            //            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){ (_) -> Void in
            self.spinner.stopAnimating()
            self.spinner.isHidden = true
            self.spinnerText.text = "Unable to Scan Wifi List"
            self.tryAgain.isHidden = false
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
